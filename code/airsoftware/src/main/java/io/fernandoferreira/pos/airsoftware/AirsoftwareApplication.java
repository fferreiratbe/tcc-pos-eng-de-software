package io.fernandoferreira.pos.airsoftware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirsoftwareApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirsoftwareApplication.class, args);
	}

}
